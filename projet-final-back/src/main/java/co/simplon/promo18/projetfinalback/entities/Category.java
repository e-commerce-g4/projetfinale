package co.simplon.promo18.projetfinalback.entities;


public class Category {
    private Integer id;
    private String label;
    
    public Category() {
    }
    public Category(String label) {
        this.label = label;
    }
    public Category(Integer id, String label) {
        this.id = id;
        this.label = label;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getLabel() {
        return label;
    }
    public void setLabel(String label) {
        this.label = label;
    }  
}
