package co.simplon.promo18.projetfinalback.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.promo18.projetfinalback.entities.User;

@Repository
public class UserRepository {

    @Autowired
    private DataSource dataSource;

    public List<User>findAll(){
        List <User> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM user");

            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                User user = new User(
                    rs.getInt("id"),
                    rs.getString("name"),
                    rs.getString("last_name"),
                    rs.getString("email"),
                    rs.getString("password"),
                    rs.getString("role"));

            list.add(user);
        }
    } catch (SQLException e) {
        e.printStackTrace();

    }
    return list;
    }

public User findById (int id){
    try(Connection connection = dataSource.getConnection()){
    PreparedStatement stmt = connection.prepareStatement("SELECT * FROM user WHERE id=?");

    stmt.setInt(1, id);

    ResultSet rs = stmt.executeQuery();

    if (rs.next()){
        User user = new User(
            rs.getInt("id"),
            rs.getString("name"),
            rs.getString("last_name"),
            rs.getString("email"),
            rs.getString("password"),
            rs.getString("role"));   
        return user;
    }
     }catch (SQLException e){
        e.printStackTrace();
    throw new RuntimeException("Database access error");
}

    return null;
    }

    public User findByEmailUser (String email){
        try(Connection connection = dataSource.getConnection()){
        PreparedStatement stmt = connection.prepareStatement("SELECT * FROM user WHERE email=?");
    
        stmt.setString(1, email);
    
        ResultSet rs = stmt.executeQuery();
    
        if (rs.next()){
            User user = new User(
                rs.getInt("id"),
                rs.getString("name"),
                rs.getString("last_name"),
                rs.getString("email"),
                rs.getString("password"),
                rs.getString("role"));   
            return user;
        }
         }catch (SQLException e){
            e.printStackTrace();
        throw new RuntimeException("Database access error");
    }
    
        return null;
        }

public void save(User user) {
    try (Connection connection = dataSource.getConnection()) {
        PreparedStatement stmt = connection.prepareStatement
        ("INSERT INTO user (name,last_name,email,password,role) VALUES (?,?,?,?,?);",
        PreparedStatement.RETURN_GENERATED_KEYS);  
    
        stmt.setString(1, user.getName());
        stmt.setString(2, user.getLast_name());
        stmt.setString(3, user.getEmail());
        stmt.setString(4, user.getPassword());
        stmt.setString(5, user.getRole());
        

        stmt.executeUpdate();
    
        ResultSet rs = stmt.getGeneratedKeys();
        if(rs.next()) {
            user.setId(rs.getInt(1));
        }
         
    } catch (SQLException e) {
            
        e.printStackTrace();
        throw new RuntimeException("Database access error");
        }
    }


public boolean update (User user){
    try (Connection connection = dataSource.getConnection()) {
        PreparedStatement stmt = connection.prepareStatement("UPDATE user SET name=?,last_name=?,email=?,password=?,role=? WHERE id=?");
 
        stmt.setString(1, user.getName());
        stmt.setString(2, user.getLast_name());
        stmt.setString(3, user.getEmail());
        stmt.setString(4, user.getPassword());
        stmt.setString(5, user.getRole());
        

        return stmt.executeUpdate() == 1;
        

    } catch (SQLException e) {
        e.printStackTrace();

    }
    return false;
}

public boolean deleteById(int id) {
    try (Connection connection = dataSource.getConnection()) {
        PreparedStatement stmt = connection.prepareStatement("DELETE FROM user WHERE id=?");

        stmt.setInt(1, id);

        return stmt.executeUpdate() == 1;

    } catch (SQLException e) {
        e.printStackTrace();

    }
    return false;
    }
}
