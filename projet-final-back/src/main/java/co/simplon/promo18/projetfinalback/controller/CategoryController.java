package co.simplon.promo18.projetfinalback.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import co.simplon.promo18.projetfinalback.entities.Category;
import co.simplon.promo18.projetfinalback.repository.CategoryRepository;

@RestController
public class CategoryController {
  
    @Autowired
    CategoryRepository repo;

    @GetMapping("/api/category")
    public List<Category> all(){
        return repo.findAll();
    }
}
