
export interface Article{
    id?:number;
    titre:string;
    ingredients:string;
    preparation: string;
    image?:string;
    date?: string;
    category?: number;
    comments?:Comment[];
    categories?:Category[];
}

export interface Comment {
    id?:number;
    comment:string;
    date: string
    comments?:Comment[];
}
export interface User{
    id?:number;
    name?:string;
    last_name?:string;
    email: string;
    password:string;
    role?: string;
}
export interface Category{
    id?:number;
    label:string;  
}
