import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { User } from '../entities';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  name = '';

  hasError = false;
  user: User = {
    name: "",
    last_name : "",
    email : "",
    password : ''
    
  }
  passwordRepeat? : string 
  constructor(private auth: AuthService, private router: Router) { }


  ngOnInit(): void {
  }


  register() {
    this.hasError = false;
    this.auth.register(this.user).subscribe({
      next: data => this.router.navigate(['/']),
      error: () => this.hasError = true
    });

  }
}

