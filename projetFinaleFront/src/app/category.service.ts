import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Category } from './entities';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private http:HttpClient) { }
  getAll(){
    return this.http.get<Category[]>(environment.apiUrl+'/api/category');
  }
  // getById(id:number) {
  //   return this.http.get<Category>('http://localhost:8080/api/category/'+id)
  // }

  // add(article:Category){
  //   return this.http.post<Category>(environment.apiUrl+'/api/category', article);
  // }

  // delete(id:number) {
  //   return this.http.delete(environment.apiUrl+'/api/category/'+id);
  // }

  // put(article:Category){
  //   return this.http.put<Category>(environment.apiUrl+'/api/category/'+article.id, article);
  // }

}
