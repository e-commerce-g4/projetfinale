import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddArticleComponent } from './add-article/add-article.component';
import { HomeComponent } from './home/home.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { RegisterComponent } from './register/register.component';
import { SingleArticleComponent } from './single-article/single-article.component';
import { UpdateArticleComponent } from './update-article/update-article.component';

const routes: Routes = [
  {path: '', component: HomeComponent },
  {path:'article/:id',component: SingleArticleComponent},
  {path:"add-article",component:AddArticleComponent},//http://localhost:4200/register
  {path:"admin/:id", component:UpdateArticleComponent},
  {path:'login', component:LoginPageComponent},
  {path:"register", component:RegisterComponent }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
