import { Component, OnInit } from '@angular/core';
import { ArticleService } from '../article.service';
import { Article, Category } from '../entities';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  selected?: Article;
  articles: Article[] = [];
// select funciona para selecionar e usar no filtro html 

  constructor(private artService: ArticleService) { }

  ngOnInit(): void {
    this.fetchAll()
  }
  fetchAll() {
    this.artService.getAll().subscribe(data => this.articles = data);
  }
  filter(idCategory: number) {
    if (idCategory) {

      this.artService.getByIdCategory(idCategory).subscribe(data => this.articles = data);
    }
    else {
      this.fetchAll()
    }
  }

  // fetchOne() {
  //   this.artService.getById(1).subscribe(data => {
  //     this.selected = data
  //     console.log(this.selected)
  //   });

  //   delete(id:number) {
  //     this.artService.delete(id).subscribe();
  //     this.removeFromList(id);
  //   }

  //   removeFromList(id: number) {
  //     this.articles.forEach((article, article_index) => {
  //       if(article.id == id) {
  //         this.articles.splice(article_index, 1);
  //       }
  //     });

  //   }
}

