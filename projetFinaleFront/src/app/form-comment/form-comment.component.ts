import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { CommentService } from '../comment.service';
import { Article, Comment } from '../entities';

@Component({
  selector: 'app-form-comment',
  templateUrl: './form-comment.component.html',
  styleUrls: ['./form-comment.component.css']
})
export class FormCommentComponent implements OnInit {
  @Input()
  article?:Article;
  comment:Comment = {
    id:0,
    comment:"",
    date: "",
};

inputComment?:string;
  @Output() 
  com = new EventEmitter();

  displayAddComment:boolean = false;
  constructor(private commentService:CommentService, private router:Router) { }

  ngOnInit(): void {
  }

  showCommentForm() {
    if (!this.displayAddComment) {
      this.displayAddComment = true;
    } else if (this.displayAddComment) {
      this.displayAddComment = false;
    }
  }
  addComment() {
    this.comment.date= new Date().toISOString().slice(0, 10);
    this.comment.comment=this.inputComment!;
    this.commentService.add(this.comment,this.article!.id!).subscribe(data=>{
      this.com.emit(this.comment),
      this.inputComment=""}); 
}
}